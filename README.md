# Git Problems and Solutions

### Use a default username and password for a local repository:

```
git config credential.helper store
git pull
```

-----

### Error: "remote: GitLab: Author '<current_author>' is not a member of team"

```
git config --global user.name "<new_name>"
git config --global user.email <example@mail.com>
git commit --amend --reset-author --no-edit
```
omit `--global` if it needs to be project-specific

-----

### Modify a specific commit:

Solution for the example commit 'bbc643cd' (the '^' points to the commit BEFORE that commit since you actually need to rebase on that one):
```
git rebase --interactive 'bbc643cd^'
```
Then modify `pick` to `edit` in the line mentioning 'bbc643cd'.
Make your changes to the file <changed file> and then:
```
git add <changed file>
git commit --amend --no-edit
git rebase --continue
```
This alters the history, so `git push --force` may be needed after that

-----

### Moving last x commits to new branch and revert master to the commit before x

Create a new branch, containing all current commits
```
git branch newbranch
```
Move master back by 3 commits (Make sure you know how many commits you need to go back)
```
git reset --keep HEAD~3
```
Go to the new branch that still has the desired commits
```
git checkout newbranch
```

-----

### Get commit diff

COMMIT is the hash of a certain commit
```
git diff COMMIT~ COMMIT
```

-----

### Delete changes of last commit (including the commit itself)

```
git reset --hard HEAD~1
```

-----

### Apply commit from other repository

add other repo with URL <url>
```
git remote add other_repo https://github.com/watabou/pixel-dungeon.git
git fetch other_repo
```
apply while in working directory 'pixel-dungeon-gdx/'
```
git --git-dir=../pixel-dungeon/.git format-patch -k -1 --stdout 66a1529002c94ffb5e22a25a5242dabd209c700c | git am --ignore-whitespace --ignore-space-change -3 -k --directory='core/'
```

-----

### Change author/email of specific commits

For example, if your commit history is A-B-C-D-E-F with F as HEAD, and you want to change the author of C and D, then you would:

```
git rebase -i C^
```
`C^` is the commit before C which is B. If you need to edit A, use `git rebase -i --root`.
Then change the lines for both C and D from pick to edit.
Once the rebase started, it would first pause at C, then you would do:
```
git commit --amend --no-edit --author="Author Name <email@address.com>"
git rebase --continue
```
It would pause again at D. Then you would repeat the previous step:
```
git commit --amend --no-edit --author="Author Name <email@address.com>"
git rebase --continue
```
The rebase would be complete and you can update your origin with the updated commits:
```
git push -f
```

-----

### View git history of files or folders

```
git log -- path/to/file
```
```
git log -- path/to/folder
git log -- path/to/folder/*
```

-----

### Squash last commits together (overwrite them by the next comit)

If your last commit (for N commits instead of 1, use `HEAD~N`) was not usable (e. g. an
intermediate state) and you want to get rid of the commit while not changing the files use:
```
git reset --soft HEAD~1
```
and then make a new commit for everything:
```
git commit
```

